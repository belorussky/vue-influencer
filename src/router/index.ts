import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/pages/Login.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('@/pages/Register.vue')
  },
  {
    path: '/',
    name: 'layout',
    component: () => import('@/pages/Layout.vue'),
    children: [
      {
        path: '',
        name: 'home',
        component: () => import('@/pages/Home.vue')
      },
      {
        path: 'rankings',
        name: 'rankings',
        component: () => import('@/pages/Rankings.vue')
      },
      {
        path: 'stats',
        name: 'stats',
        component: () => import('@/pages/Stats.vue')
      },
    ]
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
